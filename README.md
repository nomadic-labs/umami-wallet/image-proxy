[[_TOC_]]
# graphic-proxy


Simple image proxy to use [Cryptonomic](https://github.com/Cryptonomic) [Image Proxy](https://github.com/Cryptonomic/ImageProxy/) used for moderation and avoid API-key-leak.

This proxy links to Cryptonomic live deployment at https://imgproxy-prod.cryptonomic-infra.tech, using the API key provided as a parameters.

## Usage
### Start

```
yarn start
```

### Parameters

``` 
--port <port>
```
exemple: `3000`

``` 
--cryptonomickey=<api-key>
```
from: https://github.com/Cryptonomic/ImageProxy
> A live deployment of the image proxy is available for use at https://imgproxy-prod.cryptonomic-infra.tech. To request an api key, please send an email to infra@cryptonomic.tech.

### Options

With `URL` beeing the url to the image to be filtered. URL can be an `http://`, `https://` or `ipfs://` url.

```
/?url=<URL>
```


### Healthcheck

`/check` returns `0` if aboves proxy is available, `1` instead.


## Authors and acknowledgment
Nomadic-Labs (2021-2023)
* @remyzorg  Rémy El Sibaïe (2021) / Nomadic-Labs
* @comeh     Corentin Méhat (2021-2023) / Nomadic-Labs

## License
MIT

## Project status
 This project is actively used as cache and moderation within Umami backend: https://gitlab.com/nomadic-labs/umami-wallet/umami-stack-orchestration/ 